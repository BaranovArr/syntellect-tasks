// Есть строка с большим кол-вом слов через запятую (например: "кошка,собака,лошадь,корова,кошка...").
// Известно, что в строке встречаются повторяющиеся слова. 
// Нужно написать функцию на JS. На вход передаются строка с дублями, а на выходе мы получаем строку без дублей.

const input = "кошка,собака,лошадь,корова,кошка";

function stringWithoutDuplicates(input) {
  let clearedInput = new Set(input.split(','));

  return [...clearedInput].join(' ');
};

console.log(stringWithoutDuplicates(input));
