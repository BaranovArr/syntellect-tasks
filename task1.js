// Черепашке нужно забраться на вершину холма. 
// Расстояние от подножия холма до его вершины - 100м. 
// Черепашка за день залезает вверх по холму на 50м.
// Ночью она спит и скатывается на 30м вниз. 
// На какие сутки черепашка залезет на столб? 

const goal = 100;
let currentPos = 0;
let dayCounter = 0;

while (currentPos < goal) {
  dayCounter++;
  currentPos += 50;
  if (currentPos >= goal) {
    console.log(`Ответ: ${dayCounter}`); // Ответ 4
    break;
  } else {
    currentPos -= 30;
  }
}
