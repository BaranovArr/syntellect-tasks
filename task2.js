// В комнате находится человек. 
// Через какое-то время в комнату заходит еще один человек и здоровается с первым. 
// Людей в комнате становится 2, а счетчик рукопожатий становится равен 1.
//  Через какое-то время заходит еще один человек и здоровается с другими людьми в комнате. 
// Людей в комнате - 3 и счетчик рукопожатий - 3. И т.д. 
// Требуется написать код на JS для подсчета кол-ва рукопожатий для 10 человек и дать ответ.

function handShakeCounter (InitPeopleInRoom, InitCounter) {

  let numberOfPeople = InitPeopleInRoom;
  let currentShakesCounter = InitCounter;

  return function newPersonInRoom() {
    console.log('Людей в комнате: ', numberOfPeople);
    console.log('Рукопожатий: ', currentShakesCounter);
    console.log('');
    numberOfPeople++;
    currentShakesCounter = numberOfPeople - 1 + currentShakesCounter;
  }
}

let shake = handShakeCounter(1, 0); // arg1 - количество людей в комнате, arg2 - значение счетчика


for (let i = 0; i < 10; i++) {
  shake(); // Ответ 45
};
